#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel

mount /dev/block/platform/11120000.ufs/by-name/USERDATA /data

if [ ! -d /data/media/0/BatMan ]; then
	mkdir /data/media/0/BatMan
	chmod 777 /data/media/0/BatMan
fi

cp -rf /tmp/aroma /data/media/0/BatMan

find /data/media/0/BatMan/aroma -type f ! -iname "*.prop" -delete

cp -rf /tmp/aroma/.install.log /data/media/0/BatMan/aroma/install.log

exit 10

